from django import forms

class NameForm(forms.Form):
    # list_1 = forms.CharField(widget=forms.Textarea)
    # list_2 = forms.CharField(label='Your name', max_length=10000)
    userid = forms.CharField()
    password = forms.PasswordInput()

class ProfileForm(forms.Form):
    f_name = forms.CharField()
    l_name = forms.CharField()
    userid = forms.CharField()