from django.shortcuts import render,redirect
from django.http import HttpResponse
from MyNotes.models import notes
from MyNotes.models import labels
from django.conf import settings        # to get the path of setting variables for path check.
from .forms import NameForm, ProfileForm

import array as arr
from pathlib import Path                                     #for path_print.html
from os.path import abspath, basename, dirname, join, normpath  #for path_print.html
import os                                                       #for path_print.html

APPNAME = 'MyNotes'

def path_print(request):
    path_var = {'BASE_DIR' : settings.BASE_DIR, 'TEMPLATE_DIR' : settings.TEMPLATE_DIR, 'STATICFILES_DIRS' : settings.STATICFILES_DIRS, 'AUTH_PASSWORD_VALIDATORS': settings.AUTH_PASSWORD_VALIDATORS, 'STATIC_ROOT': settings.STATIC_ROOT, 'STATIC_URL': settings.STATIC_URL }
    str = ""
    str = settings.AUTH_PASSWORD_VALIDATORS
    print(str)
    return render(request, APPNAME + '/path_print.html', context = {'path_data': path_var})

def notes_view_all(request):
    all_notes = notes.objects.all()
    return render(request, APPNAME + '/notes_all.html', context={'page_note': all_notes})

def create_load(request):
    return render(request, APPNAME + '/notes.html')

def create_rec(request):
    data_text = request.POST["note_text"]
    data_name = request.POST["note_name"]
    result = notes.objects.filter(name=data_name)
    if result.count()>0:
        return HttpResponse("Name already exits.")
    elif len(data_name)==0:
        return HttpResponse("Name is required.")
    q = notes(text=data_text, name=data_name)
    q.save()
    return redirect('notes_all')

def edit_load(request):
    data_name = request.GET["name"]
    result = notes.objects.filter(name=data_name)[0]
    return render(request, APPNAME + '/edit.html', context={'page_note': result.text, 'page_name': result.name})

def edit_rec(request):
    data_text = request.POST["note_text"]
    data_name = request.POST["note_name"]
    result = notes.objects.filter(name=data_name).update(text=data_text, name=data_name)
    return redirect('notes_all')

def delete_rec(request):
    data_name = request.GET["name"]
    result = notes.objects.filter(name=data_name).delete()
    return redirect('notes_all')

def note_home(request):
    return render(request, APPNAME+'/index.html')

def format(request):
    if request.method == 'GET':
        return render(request, APPNAME + '/format.html')
    else:
        delimit_var = ","
        format_var = "any"
        input_var = request.POST["format_input"]
        pre_var = request.POST["prefix"]
        post_var = request.POST["suffix"]
        delimit_var = request.POST["delimit"]
        inline_var = None
        if "inline" in request.POST:
            inline_var = request.POST["inline"]
        if inline_var is None:
            delimit_var = delimit_var+'''\n'''
        sql_var = None
        if "SQL" in request.POST:
            sql_var = request.POST["SQL"]
            format_var = "sql"
            delimit_var = ","
        str_to_element = []
        str_to_element = input_var.splitlines()
        if sql_var:
            pre_var = post_var = "'"
            output_var = "in "
        output_var2 = []
        output_var = ''
        output_var = format_fun(str_to_element, pre_var, post_var, delimit_var, format_var)
        return render(request, APPNAME + '/format.html', context={'input_data': input_var, 'output_data': output_var, 'pre_data': pre_var, 'post_data': post_var, 'delimit_data': delimit_var, 'inline_data': inline_var, 'sql_data': sql_var})

def format_fun(list_var, pre_var, post_var, separator_var, format_var):
    var2 = []
    temp = ""
    for i in list_var:
        var2.append(pre_var + i + post_var + separator_var)
    if separator_var:
        temp = var2[-1]
        var2[-1] = temp[:-1]
    if format_var == "sql":
        var2.insert(0,"IN (")
        var2.append(")")
    return var2

def compare_list(request):
    if request.method == 'GET':
        print("get")
        return render(request, APPNAME + '/compare.html')
    else:
        input_1 = request.POST["list_input_1"]
        input_2 = request.POST["list_input_2"]
        list_1 = input_1.splitlines()
        list_2 = input_2.splitlines()
        set_1 = set(list_1)
        set_2 = set(list_2)
        common_var = set(set_1.intersection(set_2))
        common_count = len(common_var)
        unique_var = set_1.union(set_2)
        unique_count = len(unique_var)
        only1_var = set_1 - common_var
        only1_count = len(only1_var)
        return render(request, APPNAME + '/compare.html', context={'input_data_1': input_1, 'input_data_2': input_2, 'common_data': common_var, 'common_num': common_count, 'unique_data': unique_var, 'unique_num': unique_count, 'only1_data': only1_var, 'only1_num': only1_count })

def login(request):
    form = NameForm()
    #form.instance.requested_by = request.user.username
    #template_name = 'coulance_access/coulance_access.html'
    # return render(request, template_name, {'form': form})
    return render(request, APPNAME + '/login.html',{'form': form})

def profile_view(request):
    form = ProfileForm()
    return render(request, APPNAME + '/profile.html',{'form': form})
